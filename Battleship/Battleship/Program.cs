﻿using System;

namespace Battleship
{
    class Program
    {
        static void Main(string[] args)
        {
            string j1, j2, coor;
            int n1, n2, atk1, atk2, tam,c1,c2;
            int[] flota1 ;
            int[] flota2 ;

            Console.WriteLine("Bienvenido al juego del año");
            Console.WriteLine("BATALLA NAVAL");

            Console.WriteLine("Jugador 1, ingrese su nombre de comandante");
            j1 = Console.ReadLine();

            Console.WriteLine("Jugador 2, ingrese su nombre de comandante");
            j2 = Console.ReadLine();

            Console.WriteLine("Ingresar el tamaño del tablero con el cual se jugara");
            tam = int.Parse(Console.ReadLine());

            Console.WriteLine("Comandante " + j1 + " ingrese la cantidad de barcos que quiere en su flota");
            n1 = int.Parse(Console.ReadLine());
            while(n1>tam)
            {
                Console.WriteLine("Lo sentimos comandante " + j1 + ", su flota no puede exceder en tamaño al tablero");
                Console.WriteLine("Ingrese de nuevo el tamaño de su flota");
                Console.WriteLine("Considere que debe de ser menor o igual a " + tam);
                n1 = int.Parse(Console.ReadLine());
            }

            Console.WriteLine("Comandante " + j2 + " ingrese la cantidad de barcos que quiere en su flota");
            n2 = int.Parse(Console.ReadLine());
            while(n2 > tam)
            {
                Console.WriteLine("Lo sentimos comandante " + j2 + ", su flota no puede exceder en tamaño al tablero");
                Console.WriteLine("Ingrese de nuevo el tamaño de su flota");
                Console.WriteLine("Considere que debe de ser menor o igual a " + tam);
                n2 = int.Parse(Console.ReadLine());
            }
            c1 = n1;
            c2 = n2;
            flota1 = new int[tam];
            flota2 = new int[tam];
            for(int i=0; i<tam;i++)
            {
                flota1[i] = 0;
                flota2[i] = 0;
            }
            
           
            
            Console.Clear();
            Console.WriteLine("Comandante "+j1+" es momento de posicionar sus tropas");
            for(int i=0;i<=n1;i++)
            {
                for(int j=0;j<tam;j++)
                {
                    Console.WriteLine("Su barco "+ (i + 1) + " lo desea colocar en la posicion "+ (j + 1) + "?");
                    Console.WriteLine("Tecle s si es lo quiere colocar en ese posicon");
                    Console.WriteLine("En caso contrario tecle n");
                    Console.WriteLine("Tenga en cuenta que si no coloca su barco, se tomara como perdido");
                    coor = Console.ReadLine();
                    if(coor=="s")
                    {
                        if(flota1[i]==1)
                        {
                            Console.WriteLine("Lugar ya ocupado, elegir otra posicion");
                        }
                        else
                        {
                            flota1[i] = 1;
                            Console.Clear();
                            break;
                        }
                        
                    }
                }
            }

            Console.Clear();
            Console.WriteLine("Comandante " + j2 + " es momento de posicionar sus tropas");
            for (int i = 0; i <= n2; i++)
            {
                for (int j = 0; j < tam; j++)
                {
                    Console.WriteLine("Su barco " + (i+1)+ " lo desea colocar en la posicion " + (j+1) + "?");
                    Console.WriteLine("Tecle s si es lo quiere colocar en ese posicon");
                    Console.WriteLine("En caso contrario tecle n");
                    Console.WriteLine("Tenga en cuenta que si no coloca su barco, se tomara como perdido");
                    coor = Console.ReadLine();
                    if (coor == "s")
                    {
                        if (flota2[i] == 1)
                        {
                            Console.WriteLine("Lugar ya ocupado, elegir otra posicion");
                            
                        }
                        else
                        {
                            flota2[i] = 1;
                            Console.Clear();
                            break;
                        }

                    }
                }
            }
            Console.WriteLine("Ha llegado el momento de atacar");

            do
            {
                Console.WriteLine("Turno del comandante "+j1+" de atacar");
                Console.WriteLine("Ingrese la coordenada a atacar");
                atk1 = int.Parse(Console.ReadLine());
                if(flota2[atk1]==1)
                {
                    flota2[atk1] = 0;
                    Console.WriteLine("Barco hundido");
                    c2 = c2 - 1;
                }

                Console.WriteLine("Turno del comandante " + j2 + " de atacar");
                Console.WriteLine("Ingrese la coordenada a atacar");
                atk2 = int.Parse(Console.ReadLine());
                if (flota1[atk2] == 1)
                {
                    flota1[atk2] = 0;
                    Console.WriteLine("Barco hundido");
                    c1 = c1 - 1;
                }

            } while (c1 == 0 || c2 == 0);

            if(c1>c2)
            {
                Console.WriteLine("Comandante"+j1+" gana el juego");
                Console.WriteLine("Con "+c1+" barcos restantes");
            }
            else
            {
                Console.WriteLine("Comandante" + j2 + " gana el juego");
                Console.WriteLine("Con " + c2 + " barcos restantes");
            }
        }
    }
}
